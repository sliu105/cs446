import numpy as np
import keras
from keras.models import Sequential
from keras.models import Model
from keras.layers import Input, Dense, Conv3D, BatchNormalization, Activation
from keras.layers import MaxPooling3D, AveragePooling3D, Flatten, Dropout
from keras.optimizers import Adam
from keras import backend as K
from keras.regularizers import l2

# load data from files
# length of train_label / train_feature = 4602
# length of valid_feature = 1971
train_label = np.load('data/train_binary_Y.npy')
train_feature = np.load('data/train_X.npy')
valid_feature = np.load('data/valid_test_X.npy')

# normalize training data
train_feature = (train_feature - np.mean(train_feature)) / np.std(train_feature)

num_train = len(train_label)
num_valid = len(valid_feature)

# define parameters
batch_size = 128
num_classes = 19
epochs = 100

width = 26
length = 31
height = 23

# train/validation split ratio
split_ratio = 0.2
split_point = int(num_train * (1-split_ratio))

# define train & validation data
x_train = train_feature[0:split_point]
y_train = train_label[0:split_point]

x_valid = train_feature[(split_point):]
y_valid = train_label[(split_point):]

x_train = x_train.reshape(split_point, width, length, height, 1)
x_valid = x_valid.reshape(num_train - split_point, width, length, height, 1)

# convert class vectors to binary class matrices
# y_train = keras.utils.to_categorical(y_train, num_classes)
# y_valid = keras.utils.to_categorical(y_valid, num_classes)

num_filters = 64

# Start model definition.
inputs = Input(shape=(width, length, height, 1))
x = Conv2D(num_filters,
           kernel_size=7,
           padding='same',
           strides=2,
           kernel_initializer='he_normal',
           kernel_regularizer=l2(1e-4))(inputs)
x = BatchNormalization()(x)
x = Activation('relu')(x)

# Orig paper uses max pool after 1st conv.
# Reaches up 87% acc if use_max_pool = True.
# Cifar10 images are already too small at 32x32 to be maxpooled. So, we skip.

x = MaxPooling3D(pool_size=3, strides=2, padding='same')(x)
# num_blocks = 3

num_blocks = 4
num_sub_blocks = 2
# Instantiate convolutional base (stack of blocks).
for i in range(num_blocks):
    for j in range(num_sub_blocks):
        strides = 1
        is_first_layer_but_not_first_block = j == 0 and i > 0
        if is_first_layer_but_not_first_block:
            strides = 2
        y = Conv3D(num_filters,
                   kernel_size=3,
                   padding='same',
                   strides=strides,
                   kernel_initializer='he_normal',
                   kernel_regularizer=l2(1e-4))(x)
        y = BatchNormalization()(y)
        y = Activation('relu')(y)
        y = Conv3D(num_filters,
                   kernel_size=3,
                   padding='same',
                   kernel_initializer='he_normal',
                   kernel_regularizer=l2(1e-4))(y)
        y = BatchNormalization()(y)
        if is_first_layer_but_not_first_block:
            x = Conv3D(num_filters,
                       kernel_size=1,
                       padding='same',
                       strides=2,
                       kernel_initializer='he_normal',
                       kernel_regularizer=l2(1e-4))(x)
        x = keras.layers.add([x, y])
        x = Activation('relu')(x)

    num_filters = 2 * num_filters

# Add classifier on top.
x = AveragePooling3D()(x)
y = Flatten()(x)
outputs = Dense(num_classes,
                activation='softmax',
                kernel_initializer='he_normal')(y)

model = Model(inputs = inputs, outputs = outputs)
model.compile(loss=keras.losses.mean_squared_error,
              optimizer=Adam(),
              metrics=['accuracy'])

print(np.shape(x_valid),np.shape(y_valid))
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_valid, y_valid))

# score = model.evaluate(x_test, y_test, verbose=0)