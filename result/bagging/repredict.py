import os
from keras.models import model_from_json
import numpy as np
np.set_printoptions(threshold=np.nan)
# load and process testing data
test_feature = np.load('../../data/valid_test_X.npy')
num_valid = len(test_feature)

# define parameters
batch_size = 128
num_classes = 19
epochs = 35

width = 26
length = 31
height = 23

x_test = test_feature.reshape(num_valid, width, length, height, 1)

def predict(model_path, weight_path):
    # load json and reconstruct NN model
    try:
        json_file = open(model_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        print("Model loaded from json file")
    except OSError:
        print('OSError when trying to load:', model_path)
        return None

    # load weights into new model
    try:
        loaded_model.load_weights(weight_path)
        print("Weights loaded from h5 file")
    except OSError:
        print('OSError when trying to load:', weights_path)
        return None

    prediction = loaded_model.predict(x_test, batch_size = batch_size) 
    return prediction

# use all 5 models in 3 folders for bagging
folder12 = ['../12_16_4_blk', '../12_17_resnet34_small', '../12_17_resnet34_small_dropout_0.3'] 

model_list = ['model0.json', 'model1.json', 'model2.json', 'model3.json', 'model4.json']
weight_list = ['weights0.h5', 'weights1.h5', 'weights2.h5', 'weights3.h5', 'weights4.h5']
# for float point prediction
# predictions = np.zeros((num_valid, num_classes))
# for majority vote prediction
predictions1 = np.zeros((num_valid, num_classes))
for folder in folder12:
    for model, weight in zip(model_list, weight_list):
        model_path = os.path.join(folder, model)
        weight_path = os.path.join(folder, weight)
    	cur_predict = predict(model_path, weight_path)
        # predictions += cur_predict
        
        cur_predict[cur_predict>=0.5] = 1
        cur_predict[cur_predict<0.5] = 0
	# print(model_path)
	# print(cur_predict)
	# print('')
        predictions1 += cur_predict

# predictions /= 15.0
# np.save('bagging_prob', predictions)
print(predictions1)
print('###########################################')
print('')
low = predictions1 < 8
predictions1[low] = 0
high = predictions1 >= 8
predictions1[high] = 1
print(predictions1)
np.save('bagging_majority', predictions1)
