import os
from keras.models import model_from_json
import numpy as np
np.set_printoptions(threshold = np.nan)

# load and process testing data
test_feature = np.load('../../data/valid_test_X.npy')
num_valid = len(test_feature)

# define parameters
batch_size = 128
num_classes = 19
epochs = 35

width = 26
length = 31
height = 23

x_test = test_feature.reshape(num_valid, width, length, height, 1)

def predict(model_path, weight_path):
    # load json and reconstruct NN model
    try:
        json_file = open(model_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        print("Model loaded from json file")
    except OSError:
        print('OSError when trying to load:', model_path)
        return None

    # load weights into new model
    try:
        loaded_model.load_weights(weight_path)
        print("Weights loaded from h5 file")
    except OSError:
        print('OSError when trying to load:', weights_path)
        return None

    prediction = loaded_model.predict(x_test, batch_size = batch_size) 
    # print('This prediction:', prediction)
    return prediction


model_list = ['model0.json', 'model1.json', 'model2.json', 'model3.json', 'model4.json']
weight_list = ['weights0.h5', 'weights1.h5', 'weights2.h5', 'weights3.h5', 'weights4.h5']
predictions = 1.0 * np.zeros((num_valid, num_classes))
for model, weight in zip(model_list, weight_list):
	predictions += predict(model, weight)

predictions /= 5.0
print('final prediction:',predictions)
np.save('prob_prediction', predictions)
