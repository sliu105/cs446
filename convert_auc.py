import pandas as pd
import numpy as np
import os
import sys

infile = sys.argv[1]
outfile = sys.argv[2]
logs = pd.read_csv(infile)

logs['id'] = logs['id'].astype(int)

logs.to_csv(outfile, sep = ',', index = False)
