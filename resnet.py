import numpy as np
import keras
from keras.models import Sequential
from keras.models import Model
from keras.layers import Input, Dense, Conv3D, BatchNormalization, Activation
from keras.layers import MaxPooling3D, AveragePooling3D, Flatten, Dropout
from keras.optimizers import Adam
from keras import backend as K

# load data from files
# length of train_label / train_feature = 4602
# length of valid_feature = 1971
train_label = np.load('data/train_binary_Y.npy')
train_feature = np.load('data/train_X.npy')
valid_feature = np.load('data/valid_test_X.npy')

# normalize training data
train_feature = (train_feature - np.mean(train_feature)) / np.std(train_feature)

num_train = len(train_label)
num_valid = len(valid_feature)

# define parameters
batch_size = 128
num_classes = 19
epochs = 20

width = 26
length = 31
height = 23

# train/validation split ratio
split_ratio = 0.2
split_point = int(num_train * (1-split_ratio))

# define train & validation data
x_train = train_feature[0:split_point]
y_train = train_label[0:split_point]

x_valid = train_feature[(split_point):]
y_valid = train_label[(split_point):]

x_train = x_train.reshape(split_point, width, length, height, 1)
x_valid = x_valid.reshape(num_train - split_point, width, length, height, 1)

# convert class vectors to binary class matrices
# y_train = keras.utils.to_categorical(y_train, num_classes)
# y_valid = keras.utils.to_categorical(y_valid, num_classes)

num_filters = 64

inputs = Input(shape = (width, length, height, 1))
# 0th block
model = Conv3D(filters = num_filters, kernel_size = 7, strides = 2, padding= 'same')(inputs)
model = BatchNormalization()(model)
model = Activation('relu')(model)

model = MaxPooling3D(pool_size = (3, 3, 3), strides = 2, padding = 'same')(model)
print(K.int_shape(model))

# 1st block
model1 = Conv3D(filters = num_filters, kernel_size = 3, strides = 2, padding= 'same')(model)
model1 = BatchNormalization()(model1)
model1 = Activation('relu')(model1)

# print(K.int_shape(model1))

#model1 = Conv3D(filters = num_filters, kernel_size = 3, strides = 2, padding= 'same')(model1)
#model1 = BatchNormalization()(model1)
#model1 = Activation('relu')(model1)

# shortcut block
model2 = Conv3D(filters = num_filters, kernel_size = 1, strides = 2, padding= 'same')(model)

# add 1st block and shortcut block together
print(K.int_shape(model1), K.int_shape(model2))
model2 = keras.layers.add([model1, model2])

model2 = AveragePooling3D(padding= 'same')(model2)
model2 = Flatten()(model2)
outputs = Dense(num_classes, activation = 'softmax')(model2)

model = Model(inputs = inputs, outputs = outputs)
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=Adam(),
              metrics=['accuracy'])

print(np.shape(x_valid),np.shape(y_valid))
model.fit(x_train, y_train,
          batch_size=batch_size,
          epochs=epochs,
          verbose=1,
          validation_data=(x_valid, y_valid))

# score = model.evaluate(x_test, y_test, verbose=0)